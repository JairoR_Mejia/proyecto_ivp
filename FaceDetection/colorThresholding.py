import numpy as np
import matplotlib.pyplot as plt
from skimage.color import rgb2gray
import cv2


def Density(y, x, out):
    aux = 0
    for j in range(0,4):
        for i in range(0,4):
            aux = aux + out[4*y+j][4*x+i]
    return aux


def get(D, j, i):
    if(0 <= j < D.shape[0] and 0 <= i < D.shape[1]):
        return D[j][i]
    else:
        return 0


def Regulation(D, y, x):
    # Neighborhood 3x3
    points = np.zeros(9)
    points[0] = get(D,y-1,x-1)
    points[1] = get(D,y-1,x)
    points[2] = get(D,y-1,x+1)
    points[3] = get(D,y,x-1)
    points[4] = 0  # Center
    points[5] = get(D,y,x+1)
    points[6] = get(D,y+1,x+1)
    points[7] = get(D,y+1,x)
    points[8] = get(D,y+1,x+1)
    FullDensity = 0  # How many full-density points has the kernel
    for i in range(0,9):
        if(points[i]==16):
            FullDensity = FullDensity +1

    # Erosion or Dilation
    if(D[y][x]==16 and FullDensity<5):
        return 0
    elif(D[y][x]==16 and FullDensity>5):
        return 16
    elif(FullDensity>2):
        return 16
    else:
        return 0


def skinDetection(img,dirImg):
    dy = img.shape[0]
    dx = img.shape[1]
    out = np.zeros(img.shape)

    # Stages Initialization
    O1 = np.zeros((dy,dx))
    Ddy = int(dy/4)
    Ddx = int(dx/4)
    D = np.zeros((Ddy,Ddx))
    O2 = np.zeros((Ddy,Ddx))
    O3 = np.zeros((Ddy,Ddx))
    O4 = np.zeros((Ddy,Ddx))
    O5 = np.zeros((dy,dx,3))
    # YCR_CB Tranformation
    ycrcb = cv2.cvtColor(img,cv2.COLOR_BGR2YCR_CB)
    [Y, Cr, Cb] = cv2.split(ycrcb)
    # HSV Tranformation
    HSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    [H, S, V] = cv2.split(HSV)
    S = S/255.0

    # get B,G,R
    [B, G, R] = cv2.split(img)

    # FIRST STAGE
    for j in range(dy):
        for i in range(dx):
            if "alpha" in dirImg:
                if 133 <= Cr[j][i] <= 173 and 77 <= Cb[j][i] <= 127 and (0.0 <= H[j][i] <= 50.0) \
                        and (0.23 <= S[j][i] <= 0.68):
                    O1[j][i] = 1
            else:
                if 133 <= Cr[j][i] <= 173 and 77 <= Cb[j][i] <= 127:
                    O5[j][i] = [1, 1, 1]

    if "alpha" in dirImg:
        # SECOND STAGE
        # Density map
        for j in range(Ddy):
            for i in range(Ddx):
                D[j][i] = Density(j, i, O1)

        # Density Regulation
        D[:, 0] 		*= 0
        D[:, Ddx-1] 	*= 0
        D[0, :] 		*= 0
        D[Ddy-1, :] 	*= 0

        for j in range(Ddy):
            for i in range(Ddx):
                D[j][i] = Regulation(D, j, i)

        for j in range(Ddy):
            for i in range(Ddx):
                if D[j][i]==16:
                    O2[j][i] = 1
                else:
                    O2[j][i] = 0

        # THIRD STAGE
        N = 4
        for j in range(0, Ddy):
            for i in range(0, Ddx):
                block = Y[4*j:4*j+N, 4*i:4*i+N]
                block = block.ravel()
                os = np.std(block)

                if O2[j, i]==1 and os>=2:
                    O3[j, i] = 1
                else:
                    O3[j, i] = 0

        # 4TH STAGE
        for j in range(0, Ddy):
            for i in range(0,Ddx):
                if (O3[j, i] == 1 and np.sum(O3[j:j+3, i:i+3]) >=3) or (O3[j, i] == 0 and np.sum(O3[j:j+3, i:i+3])>=5):
                    O4[j, i] = 1
                else:
                    O4[j, i] = 0

        # 5TH STAGE
        O5 = np.zeros((dy, dx, 3))
        for j in range(0, Ddy):
            for i in range(0, Ddx):
                O5[4*j:4*j+4, 4*i:4*i+4] = [O4[j, i], O4[j, i], O4[j, i]]

    kernel = np.ones((5, 5), np.uint8)
    O6 = cv2.dilate(O5, kernel, iterations=1)

    # Find coordinates of the face
    block = np.where((O6[:, :, 0] == 1) & (O6[:, :, 1] == 1) & (O6[:, :, 2] == 1))
    if len(block[0]) > 0:
        box = np.array(([np.min(block[0]), np.min(block[1])], [np.max(block[0]), np.max(block[1])]))
    else:
        box = np.array(([0, 0], [0, 0]))

    out = img[:, :, :]*O6[:, :, :]

    return box, Y.astype(np.int16)


