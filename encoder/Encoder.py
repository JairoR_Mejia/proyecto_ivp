import encoder.Quantization as q
import encoder.ZigZag as zz
import encoder.Entropy as ent
import encoder.huffman as hf
import numpy as np
from scipy.fftpack import dct

# Codebook generation


def generateHuffmanCodebook(x):
    [uniques, counts] = np.unique(x, return_counts=True)
    y = []
    for i in range(0, len(uniques)):
        y.append((uniques[i], counts[i]))
    return hf.codebook(y)


def encoder(x, codebook):
    L = x.size
    y = []
    for i in range(0, L, 1):
        y.append(codebook.get(x[i]))
    return y


def encode(image, coordinates, current_img):
    adaptative = 0  # Flag to indicate that a face has been found
    min_point = coordinates[0]		# min[y,x]
    max_point = coordinates[1]		# max[y,x]

    # Variable Init
    dct_image = np.zeros(shape=image.shape, dtype=image.dtype)
    quantised = np.zeros(shape=image.shape, dtype=image.dtype)
    zig_zag = np.zeros(shape=image.shape, dtype=image.dtype)
    # Block Processing
    for i in range(0, image.shape[0], 8):
        for j in range(0, image.shape[1], 8):
            if ((i+8) >= min_point[0] or i >= min_point[0]) and (i <= max_point[0] or (i+8) <= max_point[0]):		# Detect Face
                if ((j+8) >= min_point[1] or j >= min_point[1]) and (j <= max_point[1] or (j+8) <= max_point[1]):
                    adaptative = 1
            dct_image[i:i+8, j:j+8] = dct(image[i:i+8, j:j+8])
            quantised[i:i+8, j:j+8] = q.quantization(dct_image[i:i+8, j:j+8], adaptative)
            zig_zag[i:i+8, j:j+8] = zz.zigzag(quantised[i:i+8, j:j+8])
            adaptative = 0

    codebook = generateHuffmanCodebook(zig_zag)

    encoded = encoder(np.reshape(zig_zag, zig_zag.shape[0]*zig_zag.shape[1]), codebook)
    encoded = np.reshape(encoded, newshape=zig_zag.shape)

    print(str(ent.H(encoded)) + "		" + str(ent.H(current_img)))

