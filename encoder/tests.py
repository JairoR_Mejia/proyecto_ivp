import encoder.DCT as edct
import matplotlib.pyplot as plt
import encoder.Encoder as enc
import encoder.Entropy as ent
import cv2
from MotionCompensation.MotionCompensation import motion_compensation

baboon = cv2.imread("../Source/gamma/img_3.bmp")
baboon2 = cv2.imread("../Source/gamma/img_4.bmp")

gray = cv2.cvtColor(baboon, cv2.COLOR_BGR2GRAY)
gray2 = cv2.cvtColor(baboon2, cv2.COLOR_BGR2GRAY)

error = motion_compensation(gray, gray2)

H = ent.H(gray)
print(H)
enc.encode(error, [[0, 0], [0, 0]], gray)

# test = edct.encode(baboon)
#
# plt.title("Baboon")
# plt.imshow(test, cmap=plt.cm.gray)
# plt.xticks([])
# plt.yticks([])
# plt.show()

# vector = [1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 2, 4, 5, 7, 3, 7, 3, 5, 2, 6, 7, 9, 0, 5, 3, 5]
# vector = [1, 1, 1, 1, 1, 0, 0, 0, 2, 4, 4, 4, 3, 3]
# vector = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6]
# codebook = hf.generate_codebook(vector)
# code = hf.encode(codebook, vector)
#print(code)