import numpy as np
from scipy.fftpack import dct, idct


def encode(image):
    N = 8  # Block of size NxN
    new = np.zeros(image.shape)
    for i in range(0, image.shape[0], N):
        for j in range(0, image.shape[1], N):
            new[i:i+8, j:j+8] = dct(image[i:i+8, j:j+8])

    return new
