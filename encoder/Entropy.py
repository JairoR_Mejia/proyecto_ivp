import numpy as np


def H(image):
    pixel_value, freq_value = np.unique(image, return_counts=True)
    prob = freq_value/(image.shape[0]*image.shape[1])
    H = -sum(prob[np.where(prob!=0)]*np.log2(prob[np.where(prob!=0)]))
    return H
