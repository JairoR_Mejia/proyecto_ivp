import numpy as np


def quantization(image, adaptative):

    # Quantisation Matrix (coarse)
    qc = np.matrix([[16, 11, 10, 16, 24, 40, 51, 61],
                    [12, 12, 14, 19, 26, 58, 60, 55],
                    [14, 13, 16, 24, 40, 57, 69, 56],
                    [14, 17, 22, 29, 51, 87, 80, 62],
                    [18, 22, 37, 56, 68, 109, 103, 77],
                    [24, 35, 55, 64, 81, 104, 113, 92],
                    [49, 64, 78, 87, 103, 121, 120, 101],
                    [72, 72, 95, 98, 112, 100, 103, 99]])

    # Quantisation Matrix (fine)
    qf = np.matrix([[1, 1, 1, 1, 1, 40, 51, 61],
                    [1, 1, 1, 1, 26, 58, 60, 55],
                    [1, 1, 1, 24, 40, 57, 69, 56],
                    [1, 1, 22, 29, 51, 87, 80, 62],
                    [1, 22, 37, 56, 68, 109, 103, 77],
                    [24, 35, 55, 64, 81, 104, 113, 92],
                    [49, 64, 78, 87, 103, 121, 120, 101],
                    [72, 72, 95, 98, 112, 100, 103, 99]])

    if adaptative == 1:
        q = qf
    else:
        q = qc

    quantised = np.zeros(image.shape)

    for i in range(8):
        for j in range(8):
            quantised[i, j] = round(image[i, j]/q[i, j])

    return quantised
