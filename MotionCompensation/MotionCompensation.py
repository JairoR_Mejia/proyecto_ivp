import math
import numpy as np
from PIL import Image
import MotionCompensation.imageReader as imageReader
import MotionCompensation.fullSearch as fullSearch
import MotionCompensation.diamondSearch as diamondSearch


def motion_compensation(referenced_picture, current_picture):
    p = 4  # len(current_picture)/70 # half of search window
    n = 4  # size of macroblock
    motion_estimation = None
    comparations = None
    useInterpolation = False
    # DIAMOND METHOD
    if useInterpolation:
        print("---Diamond search doesn't support interpolation---")
    useInterpolation = False
    ds = diamondSearch.DiamondSearch(current_picture, referenced_picture, n, p)
    compressedImage = ds.createCompressedImage()

    # FULL METHOD
    # full_ = fullSearch.FullSearch(current_picture=current_picture,referenced_picture=referenced_picture,n=n,p=p,useIntrpolation=useInterpolation)
    # compressedImage = full_.createCompressedImage()

    im = Image.new("L", (len(compressedImage[0]), len(compressedImage)), "white")
    img_list = []
    for lst in compressedImage:
        img_list = img_list + lst
    im.putdata(img_list)

    prediction = np.array(im)
    error = prediction - current_picture
    return error
