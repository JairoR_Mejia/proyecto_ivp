import cv2
from FaceDetection import skinDetection
from encoder.Encoder import encode
from MotionCompensation.MotionCompensation import motion_compensation
import matplotlib.pyplot as plt
import os

referenced_picture = []
current_picture = []

dirImg = "Source/alpha/"					# Change directory to access another video
frames = len(os.listdir(dirImg))			# How many frames has the video
img_previous = []

for i in range(1, frames):
    name = "img_" + str(i) + ".ppm"
    img = cv2.imread(dirImg+name)  # Save the image in BGR order
    [box, img_current] = skinDetection(img, dirImg)
    if i > 2:							# motion compensation
        # TODO: Improve motion compensation, the error in the error images gives a bad value of entropy for some images
        error = motion_compensation(img_previous, img_current)
        plt.imshow(error)
        plt.show()
        encode(error, box, img_current)		# encode
    img_previous = img_current


"""
b,g,r = cv2.split(skin)       	 			# get b,g,r
rgb_img = cv2.merge([r,g,b])				# switch it to rgb
Image.fromarray(rgb_img).save('Prueba_3.png');
#"""

