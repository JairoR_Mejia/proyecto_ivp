# README #

This README documents the steps to run the application "Adaptive Encoding of Head and Shoulders Videos by Detecting Face".

### What is this repository for? ###

This repository contains the timeline and specific work done by each group member. Each contribution can be seen as a commit of the diferente
stages included in this application process. The project was done considering the git flow. Each stage was done in a separate branch until its
performance was the better (including behavior with other modules).

Adaptative encoding v1.0.0

### How do I get set up? ###

* **Summary of set up:**
	* _From Linux:_ python main.py.
	* _From Pycharm:_ right click on the main.py file and then click on "run"-option


* **Configuration:**
You need to install opencv, skimage, matplotlib and scipy in your computer to be able to run the code

* **Dependencies:**
Python libraries like numpy, opencv, skimage, matplotlib and scipy are needed to run the code

* **References:**
	* Chai, D. and Ngan, K. (1999). Face segmentation using skin-color map in videophone applications. IEEE Transactions on Circuits and Systems for Video Technology, 9(4), pp.551-564.
	* Gao, W. and Ma, S. (2015). Advanced video coding systems. Cham: Spring International  Publishing, Switzerland.
	* GitHub. (2018). DavidSie/MotionVectorEstimator. [online] Available at: https://github.com/DavidSie/MotionVectorEstimator [Accessed 14 Jan. 2018].
	* Kolkur, S., Kalbande, D., Shimpi, P., Bapat, C., & Jatakia, J. (2017). Human Skin Detection Using RGB, HSV and YCbCr Color Models. Proceedings of the International Conference on Communication and Signal Processing 2016 (ICCASP 2016). doi:10.2991/iccasp-16.2017.51


* **How to run tests:**
	* _From Pycharm:_ go to encoder folder and run the code on file tests.py using the IDE run tool. 
	* _From Linux:_ go to encoder folder and write in the console: python tests.py


* **Deployment instructions:**
	* _From Linux:_ python main.py.
	* _From Pycharm:_ right click on the main.py file and then click on "run"-option


### Contribution guidelines ###

* **Tests:**
The encoder has a test file which contains a test image and apply all the process, i.e DCT, Quantisation and Huffman coding.

* **Code review:**
Each part of code was reviewed by each member. Some corrections were made during the developing process.

* **Pitch and poster presentation:**
The pitch and poster presantation was done by Cesar Quintero Arias during the lecture "Image, Video and Perception" at the TU Ilmenau


### Who do I talk to? ###

* Cesar Quintero Arias: 	cequinteroar@unal.edu.co (cesar.quintero-arias@tu-ilmenau.de) 						Encoding
* Jairo Mejia Aponte: 		jrmejiaa@unal.edu.co (jairo-rodrigo.mejia-aponte@tu-ilmenau.de)    					Skin Detection
* Heidy Cardenas Poveda: 	hccardenasp@unal.edu.co (heidy-catherine.cardenas-poveda@tu-ilmenau.de)  			Motion compensation